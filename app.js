const {sequelize, User, Post} = require('./models');

const express = require('express');

const app = express();

app.use(express.json());

app.post('/users', async(req, res) => {
    const { name, email, role } = req.body;
    //create uuid and pass it here
    try {
        const user = await User.create ({ name, email, role});
        // await user.destroy();
        return res.json(user);
    } catch (error) {
        return res.status(500).json(err);
    }
})

app.get ('/users', async (req, res) => {
    try {
        const users = await User.findAll();
        return res.json(users);
    } catch (error) {
        console.log(err);
        return res.status(500).json({
            error: "Something went wrong"
        })
    }
})

app.get ('/users/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    console.log(uuid);
    try {
        const users = await User.findOne ({
            where: {
                uuid
            }
        })
        return res.json(users);
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            error: "Something went wrong"
        })
    }
})

app.post ('/posts', async (req, res) => {
    const { userUuid, body } = req.body;
    try {
        const user = await User.findOne({
            where: {
                uuid: userUuid
            }
        })
        const post = await Post.create({
            body, userId: user.id,
        })
        return res.json(post);

    } catch (error) {
        console.log(error);
        return res.status(500).json(err);
    }
})

app.get ('/posts', async (req, res) => {
    try {
        const posts = await Post.findAll({
            include: [User],
        });
        return res.json(posts);

    } catch (error) {
        console.log(error);
        return res.status(500).json(err);
    }
})

app.listen ({
    port: 5000
}, async () => {
    console.log('Server up on http://localhost:5000');
    await sequelize.authenticate();
    console.log('Database Connected');
})  

